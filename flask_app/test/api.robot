*** Settings ***
Resource    keyword.resource
Library    JSONLibrary
Library    json

# Suite Setup    Run Keyword     Init App # что-то оно не работает
# Suite Teardown    Run Keyword    Down App # что-то оно не работает

***Variables***

${live_server}=    False

*** Test Cases ***

Alive Server
    [Documentation]    Проверяет запустился ли сервер
    ${live_server_local}=    Test Server Alive
    Set Global Variable    ${live_server}    ${live_server_local}
    Should Be True    ${live_server}

Test instance 1
    [Documentation]    Проверка работоспособности требования 1 
    
    IF    ${live_server} == True
        Test Text Requests
    ELSE
        Skip    ${live_server}
    END

Test instance 2
    [Documentation]    Проверка работоспособности требования 2
    
    IF    ${live_server} == True
        Test File Requests
    ELSE
        Skip
    END