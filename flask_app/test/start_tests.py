from time import gmtime, strftime
import sys
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-c",action="store_true")
parser.add_argument("-ca",action="store_true")

args = parser.parse_args()

if args.c:
    os.system("rm -rf ./results/*")
    dts=strftime("%Y.%m.%d.%H.%M.%S", gmtime())
    cmd=f"robot -d results/test_{dts}/ api.robot"
    os.system(cmd)
elif args.ca:
    os.system("rm -rf ./results/*")
else:
    dts=strftime("%Y.%m.%d.%H.%M.%S", gmtime())
    cmd=f"robot -d results/test_{dts}/ api.robot"
    os.system(cmd)
    