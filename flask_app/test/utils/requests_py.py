import requests
import json

def get(url:str) -> None:
    try:
        result = requests.get(url)
        
    except Exception as error:
        result = error
        return [-1, result]
    else:
        return [0, result]

def get_file(url:str,file_name:str) -> None:

    try:
        url = url + "/"+ file_name
        result = requests.get(url)
        status_code = result.status_code
    except Exception as error:
        result = error
        return [-1, result]
    else:
        if status_code == 200:
            with open(f"media/{file_name}","wb") as file:
                file.write(result.content)
                return [0, result]
        else:
            return [0, result]

def get_text(url:str,text:str):
    try:
        url = url + "/" + text 
        result = requests.get(url)
        
    except Exception as error:
        result = error
        return [-1, result]
    else:
        return [0, result]

def post(url:str,path_to_json) -> None:
    with open(path_to_json,encoding="utf-8") as file:
        jsonchick = json.load(file)
        try:
            result = requests.post(url,json=jsonchick)
        except Exception as error:
            rc = result
            result = error
            return [-1, result]
        else:
            return [0, result, result.json]


if __name__ == "__main__":
    print(get('http://127.0.0.1:5000'))
    print(get('http://127.0.0.1:5000/get_text/asdf'))
    print(post("http://127.0.0.1:5000/get_json","body.json"))