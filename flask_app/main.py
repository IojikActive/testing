import os 

from flask import Flask
from flask import send_file
from flask import abort

app = Flask("testing_app")

@app.route("/",methods=["GET"])
def welcome_page():
    return "<p>Hello, World!</p>"

@app.route("/get_text/<_text>",methods=["GET"])
def get_text(_text:str):
    return str(_text)

@app.route("/get_file/<file_name>",methods=["GET"])
def get_file(file_name:str):    
    if os.path.isfile(f"media/{file_name}"):
        return send_file(f"media/{file_name}")
    else:
        abort(404)


if __name__ == "__main__":
    app.run()